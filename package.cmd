@echo off

set ROOT=%~dp0%
set PATH=%ROOT%bin;%PATH%
set MODE=%1
set INNO_COMPILER=C:\Program Files (x86)\Inno Setup 5\Compil32.exe
pushd "%ROOT%"

setlocal
  call build_%MODE%_x64.cmd
endlocal

pushd 3rd_party
    rd /s /q sia* > NUL
    sleep 2
    unzip Sia.zip || goto :ERROR
    sleep 5
    move Sia-* sia || goto :ERROR
popd

"%INNO_COMPILER%" /cc SiaDrive_Packager_%MODE%.iss || goto :ERROR

goto :END

:ERROR
  pause
  popd
  exit 1

:END
  popd