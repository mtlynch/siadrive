#ifndef _SIADOKANDRIVE_H
#define _SIADOKANDRIVE_H

#ifdef _WIN32
#define WIN32_NO_STATUS

// Import or export functions and/or classes
#ifdef SIADRIVE_DOKAN_EXPORT_SYMBOLS
#define SIADRIVE_DOKAN_EXPORTABLE __declspec(dllexport)
#else
#define SIADRIVE_DOKAN_EXPORTABLE __declspec(dllimport)
#endif //SIADRIVE_DOKAN_EXPORT_SYMBOLS
#else
#define SIADRIVE_DOKAN_EXPORTABLE
#endif

#include <siaapi.h>
#include <mutex>
#include <eventsystem.h>
#include <uploadmanager.h>

NS_BEGIN(Sia)
  NS_BEGIN(Api)
    NS_BEGIN(Dokan)
      
      class SIADRIVE_DOKAN_EXPORTABLE SiaDokanDriveException :
          std::exception {
      public:
        SiaDokanDriveException(char *message) :
            std::exception(message) {
          
        }
      };
      
      class SIADRIVE_DOKAN_EXPORTABLE CSiaDokanDrive {
      public:
        // throws SiaDokenDriveException
        CSiaDokanDrive(std::shared_ptr<CSiaApi> siaApi, std::shared_ptr<CSiaDriveConfig> siaDriveConfig);
      
      public:
        ~CSiaDokanDrive();
      
      private:
        std::shared_ptr<CSiaApi> _siaApi;
        std::shared_ptr<CSiaDriveConfig> _siaDriveConfig;
        std::mutex _startStopMutex;
      
      public:
        void ClearCache();
        
        std::shared_ptr<json> GetUploadFileList() const;
        
        bool IsMounted() const;
        
        void Mount(const wchar_t &driveLetter, const SString &cacheLocation, const std::uint64_t &maxCacheSizeBytes);

        void NotifyOnline();

        void NotifyOffline();

        void Unmount(const bool &clearCache = false);
        
        void Shutdown();
      };

NS_END(3)
#endif //_SIADOKANDRIVE_H