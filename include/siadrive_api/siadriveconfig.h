#pragma once

#include <siacommon.h>

NS_BEGIN(Sia)
  NS_BEGIN(Api)
    
    class SIADRIVE_EXPORTABLE CSiaDriveConfig {
    public:
      CSiaDriveConfig(const bool &autoSave);
      
      CSiaDriveConfig(const bool &autoSave, const SString &filePath);
      
      explicit CSiaDriveConfig(const json &configDoc);
    
    public:
      ~CSiaDriveConfig();
    
    Property(SString, FilePath, public, private)
    
    JProperty(std::string, Renter_UploadDbFilePath, public, private, _configDocument)
    
    JProperty(std::string, TempFolder, public, private, _configDocument)
    
    JProperty(std::string, CacheFolder, public, public, _configDocument)
    
    JProperty(std::uint16_t, ApiPort, public, public, _configDocument)
    
    JProperty(std::uint16_t, HostPort, public, public, _configDocument)
    
    JProperty(std::uint16_t, RpcPort, public, public, _configDocument)
    
    JProperty(std::uint8_t, MaxUploadCount, public, public, _configDocument)
    
    JProperty(std::string, HostNameOrIp, public, public, _configDocument)
    
    JProperty(bool, LockWalletOnExit, public, public, _configDocument)
    
    JPropertyCb(bool, AutoStartOnLogon, public, public, _configDocument, OnAutoStartOnLogonChanged)
    
    JProperty(bool, LaunchBundledSiad, public, public, _configDocument)
    
    JProperty(bool, LaunchFileMgrOnMount, public, public, _configDocument)
    
    JProperty(std::string, EventLevel, public, public, _configDocument)
    
    JProperty(bool, ConfirmApplicationExit, public, public, _configDocument)
    
    JProperty(bool, CloseToTray, public, public, _configDocument)
    
    JProperty(bool, StoreUnlockPassword, public, public, _configDocument)
    
    JProperty(bool, AutoMountOnUnlock, public, public, _configDocument)
    
    JProperty(std::string, LastMountLocation, public, public, _configDocument)
    
    private:
      const bool _autoSave;
      json _configDocument;
    
    private:
      bool LoadDefaults();
      
      void Load();
      
      void OnAutoStartOnLogonChanged(const bool &value);
      
      void Save() const;
    
    
    public:
      SiaHostConfig GetHostConfig() const;
      
      SString ToString() const;
    };

NS_END(2)