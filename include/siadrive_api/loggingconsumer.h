#ifndef _LOGGINGCONSUMER_H
#define _LOGGINGCONSUMER_H

#include <siacommon.h>
#include <filepath.h>

NS_BEGIN(Sia)
  NS_BEGIN(Api)
    
    class CEvent;
    
    enum class EventLevel;
    
    class SIADRIVE_EXPORTABLE CLoggingConsumer {
    public:
      CLoggingConsumer(const EventLevel &eventLevel);
    
    public:
      ~CLoggingConsumer();
    
    Property(EventLevel, EventLevel, public, public)
    
    private:
      FILE* _logFile = nullptr;
      FilePath _logPath;

    private:
      void CheckLogRoll(const size_t& count);
      void CloseLogFile();
      void ProcessEvent(const CEvent &eventData);
      void ReOpenLogFile();
    };

NS_END(2)
#endif //_LOGGINGCONSUMER_H