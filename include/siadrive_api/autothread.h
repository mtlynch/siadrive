#ifndef _AUTOTHREAD_H
#define _AUTOTHREAD_H

#include <siacommon.h>
#include <condition_variable>

NS_BEGIN(Sia)
  NS_BEGIN(Api)
    
    class CSiaDriveConfig;
    
    class CSiaCurl;
    
    class SIADRIVE_EXPORTABLE CAutoThread {
    public:
      CAutoThread(std::shared_ptr<CSiaDriveConfig>siaDriveConfig);
      
      CAutoThread(std::shared_ptr<CSiaDriveConfig>siaDriveConfig, std::function<void(std::shared_ptr<CSiaDriveConfig>)> autoThreadCallback);
    
    public:
      virtual ~CAutoThread();
    
    private:
      bool _stopRequested;
      std::shared_ptr<CSiaDriveConfig>_siaDriveConfig;
      std::function<void(std::shared_ptr<CSiaDriveConfig>)> _AutoThreadCallback;
      std::unique_ptr<std::thread> _thread;
      std::mutex _startStopMutex;
      std::mutex _stopMutex;
      std::condition_variable _stopEvent;
    
    protected:
      virtual void AutoThreadCallback(std::shared_ptr<CSiaDriveConfig>siaDriveConfig);
    
    public:
      bool IsRunning() const;
      
      virtual void StartAutoThread();
      
      virtual void StopAutoThread();
    };

NS_END(2)
#endif //_AUTOTHREAD_H