@echo off
set ROOT=%~dp0%

pushd "%ROOT%"

  call build_debug_x64.cmd

  pushd "%ROOT%dist\Debug"
    start siadrive.exe
  popd

popd