@echo off
set ROOT=%~dp0%

pushd "%ROOT%"

  call build_release_x64.cmd

  pushd "%ROOT%dist\Release"
    start siadrive.exe
  popd

popd