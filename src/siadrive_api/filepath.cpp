#include <filepath.h>
#include <siacommon.h>

#ifdef __linux__
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <libgen.h>
#endif

#ifdef _WIN32
#include <Shlobj.h>
#endif

using namespace Sia::Api;

SString FilePath::FinalizePath(const SString& path) {
  regex r((NDirSep + "+").str());
  const SString str = std::regex_replace(path.str(), r, DirSep.str());
  regex r2((DirSep + DirSep + "+").str());
  SString ret = std::regex_replace(str.str(), r2, DirSep.str());
  if ((ret.Length() > 1) && (ret[ret.Length() - 1] == DirSep[0])) {
    ret = ret.SubString(0, ret.Length() - 1);
  }

  return ret;
}

SString FilePath::GetTempDirectory() {
  // TODO path temp_directory_path();  path temp_directory_path(std::error_code& ec); from c++17

#ifdef _WIN32
  SString tempPath;
  tempPath.Resize(MAX_PATH + 1);
  GetTempPath(MAX_PATH, &tempPath[0]);
  return std::move(tempPath.Fit());
#else
  return P_tmpdir;
#endif
}

SString FilePath::GetAppDataDirectory() {
#ifdef _WIN32
  ComInitWrapper cw;

  PWSTR localAppData = nullptr;
  ::SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, nullptr, &localAppData);
  SString ret = localAppData;
  ::CoTaskMemFree(localAppData);

  return ret;
#else
  struct passwd *pw = getpwuid(getuid());
  FilePath ret = SString(pw->pw_dir);
  ret.Append(".config");
  return ret;
#endif
}

FilePath::FilePath() {

}

FilePath::FilePath(const FilePath& filePath) :
  _path(filePath._path) {
}

FilePath::FilePath(const SString& path) {
  _path = FinalizePath(path);
}

FilePath::FilePath(const FilePath& filePath1, const FilePath& filePath2) {
  _path = filePath1;
  Append(filePath2);
}

FilePath::FilePath(const FilePath& filePath1, const SString& path2) {
  _path = filePath1;
  Append(FinalizePath(path2));
}

FilePath::FilePath(const SString& path1, const FilePath& filePath2) {
  _path = FinalizePath(path1);
  Append(filePath2);
}

FilePath::FilePath(const SString& path1, const SString& path2) {
  _path = FinalizePath(path1);
  Append(FinalizePath(path2));
}

FilePath::FilePath(FilePath&& filePath) :
  _path(std::move(filePath._path)) {
}

FilePath::~FilePath() {
}

#ifdef _WIN32
const SString FilePath::DirSep = "\\";
const SString FilePath::NDirSep = "/";
#else
const SString FilePath::DirSep = "/";
const SString FilePath::NDirSep = "\\";
#endif

FilePath& FilePath::Append(const FilePath& filePath) {
  _path = FinalizePath(_path + DirSep + filePath);
  return *this;
}

FilePath& FilePath::Append(const SString& path) {
  _path = FinalizePath(_path + DirSep + path);
  return *this;
}

bool FilePath::IsDirectory() const {
#ifdef _WIN32
  return ::PathIsDirectory(&_path[0]) ? true : false;
#else
  struct stat sb;
  return (!stat(&_path[0], &sb) && S_ISDIR(sb.st_mode));
#endif
}

bool FilePath::IsFile() const {
#ifdef _WIN32
  return ::PathFileExists(&_path[0]) ? true : false;
#else
  struct stat sb;
  return (!stat(&_path[0], &sb) && !S_ISDIR(sb.st_mode)); // TODO Or S_ISREG????
#endif
}

bool FilePath::IsUNC() const {
#ifdef _WIN32
  return ::PathIsUNC(&_path[0]) ? true : false;
#else
  return false; // Not really supported on non-Windows
#endif
}

bool FilePath::CreateDirectory() const {
  SString path = FilePath(_path).MakeAbsolute();
#ifdef _WIN32
  return (::SHCreateDirectory(nullptr, &path[0]) == ERROR_SUCCESS);
#else
  bool ret = true;
  const auto paths = path.Split(FilePath::DirSep[0]);
  for (auto i = 0; ret && (i < paths.size()); i++) {
    if (paths[i].Length()) { // Skip root
      const auto status = mkdir(&path[0], S_IRWXU);
      ret = ((status == 0) || (errno == EEXIST));
    }
  }
  return ret;
#endif
}

bool FilePath::RemoveDirectory() const {
#ifdef _WIN32
  return !IsDirectory() || ::RemoveDirectory(&_path[0]) ? true : false;
#else
  return !IsDirectory() || (rmdir(&_path[0]) == 0);
#endif
}

bool FilePath::DeleteFile() const {
#ifdef _WIN32
  return !IsFile() || ::DeleteFile(&_path[0]) ? true : false;
#else
  return !IsFile() || (unlink(&_path[0]) == 0);
#endif
}

bool FilePath::MoveFile(const FilePath& filePath) {
  FilePath folder(filePath);
  folder.RemoveFileName().CreateDirectory();
#ifdef _WIN32
  return ::MoveFile(&_path[0], &filePath[0]) ? true : false;
#else
  return (rename(&_path[0], &filePath[0]) == 0);
#endif
}

const SString& FilePath::GetPath() const {
  return _path;
}

FilePath& FilePath::RemoveFileName() {
#ifdef _WIN32
  ::PathRemoveFileSpec(&_path[0]);
  _path = _path.str().c_str();
#else
  SString tmp(_path);
  _path = dirname(&tmp[0]);
#endif
  return *this;
}

bool FilePath::operator==(const FilePath& filePath) const {
  if (this != &filePath) {
    return filePath._path == _path;
  }

  return true;
}

bool FilePath::operator==(FilePath&& filePath) const {
  if (this != &filePath) {
    return filePath._path == _path;
  }

  return true;
}

bool FilePath::operator!=(const FilePath& filePath) const {
  if (this != &filePath) {
    return filePath._path != _path;
  }

  return false;
}

bool FilePath::operator!=(FilePath&& filePath) const {
  if (this != &filePath) {
    return filePath._path != _path;
  }

  return false;
}

FilePath& FilePath::operator=(const FilePath& filePath) {
  if (this != &filePath) {
    _path = filePath._path;
  }

  return *this;
}

FilePath& FilePath::operator=(FilePath&& filePath) {
  if (this != &filePath) {
    _path = std::move(filePath._path);
  }

  return *this;
}

SString::SChar& FilePath::operator[](const size_t& idx) {
  return _path[idx];
}

const SString::SChar& FilePath::operator[](const size_t& idx) const {
  return _path[idx];
}

FilePath::operator SString() const {
  return _path;
}

FilePath& FilePath::MakeAbsolute() {
#ifdef _WIN32
  if (::PathIsRelative(&_path[0])) {
    SString temp;
    temp.Resize(MAX_PATH + 1);
    _path = _wfullpath(&temp[0], &_path[0], MAX_PATH);
  }
#else
  char* full = realpath(&_path[0], nullptr);
  _path = full;
  free(full);
#endif

  return *this;
}

FilePath& FilePath::StripToFileName() {
#ifdef _WIN32
  _path = ::PathFindFileName(&_path[0]);
#else
  SString tmp(_path);
  _path = basename(&tmp[0]);
#endif

  return *this;
}

bool FilePath::CreateEmptyFile() {
  FILE* fp = fopen(SString::ToUtf8(_path).c_str(), "w+");
  if (fp) {
    fclose(fp);
    return true;
  }

  return false;
}

FilePath& FilePath::Resolve() {
  _path = FinalizePath(_path.Replace("~", GetAppDataDirectory()));
  return *this;
}

bool FilePath::IsParentOf(const FilePath& filePath) const {
  return filePath._path.BeginsWith(_path + DirSep);
}
