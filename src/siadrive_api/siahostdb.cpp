#include <siaapi.h>
#include <siadriveconfig.h>

using namespace Sia::Api;

CSiaApi::_CSiaHostDb::_CSiaHostDb(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  CSiaBase(siaDriveConfig) {
}

CSiaApi::_CSiaHostDb::~_CSiaHostDb() {
}

void CSiaApi::_CSiaHostDb::Refresh() {
  CSiaHostCollectionPtr hostCollection(new CSiaHostCollection());
  json result;
  if (ApiSuccess(CSiaCurl(GetSiaDriveConfig()->GetHostConfig()).Get(L"/hostdb/active", result))) {
    auto hosts = result["hosts"];
    for (const auto& host : hosts) {
      CSiaHostPtr hostPtr(new CSiaHost(GetSiaDriveConfig(), host));
      hostCollection->push_back(hostPtr);
    }
  }
  _activeHosts = hostCollection;
}

CSiaHostCollectionPtr CSiaApi::_CSiaHostDb::GetActiveHosts() const {
  return _activeHosts;
}
