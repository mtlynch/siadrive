#include <loggingconsumer.h>
#include <eventsystem.h>

using namespace Sia::Api;

CLoggingConsumer::CLoggingConsumer(const EventLevel &eventLevel) :
    _EventLevel(eventLevel) {
  _logPath = FilePath("~\\siadrive\\logs\\siadrive.log").Resolve();
  FilePath(_logPath).RemoveFileName().CreateDirectory();
  CheckLogRoll(0);
  ReOpenLogFile();
  CEventSystem::EventSystem.AddEventConsumer([=](const CEvent &event) {
    this->ProcessEvent(event);
  });
}

CLoggingConsumer::~CLoggingConsumer() {
  CloseLogFile();
}

void CLoggingConsumer::CloseLogFile() {
  if (_logFile) {
    fclose(_logFile);
    _logFile = nullptr;
  }
}

void CLoggingConsumer::CheckLogRoll(const size_t &count) {
  if ((FileSize(_logPath) + count) >= (1024 * 1024 * 5)) {
    CloseLogFile();
    for (int i = 5; i > 0; i--) {
      auto fp = FilePath("~\\siadrive\\logs\\siadrive." + SString::FromUInt8(i) + ".log").Resolve();
      if (fp.IsFile()) {
        if (i == 5) {
          fp.DeleteFile();
        } else {
          auto nextFp = FilePath("~\\siadrive\\logs\\siadrive." + SString::FromUInt8(i + 1) + ".log").Resolve();
          fp.MoveFile(nextFp);
        }
      }
    }
  
    FilePath(_logPath).MoveFile(FilePath("~\\siadrive\\logs\\siadrive.1.log").Resolve());
    ReOpenLogFile();
  }
}

void CLoggingConsumer::ReOpenLogFile() {
  CloseLogFile();
#ifdef _WIN32
  _logFile = _fsopen(SString::ToUtf8(static_cast<SString>(_logPath)).c_str(), "a+", _SH_DENYWR);
#else
  _logFile = fopen(SString::ToUtf8(static_cast<SString>(_logPath)).c_str(), "a+");
#endif
}

void CLoggingConsumer::ProcessEvent(const CEvent &eventData) {
  if (eventData.GetEventLevel() <= GetEventLevel()) {
    std::time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::stringstream ss;
    ss << std::put_time(std::localtime(&now), "%F %T ");
  
    bool retry = false;
    const std::string msg = SString::ToUtf8(ss.str() + eventData.GetSingleLineMessage() + "\n");
    CheckLogRoll(msg.length());
    do {
      if (fwrite(&msg[0], 1, msg.length(), _logFile) != msg.length()) {
        ReOpenLogFile();
        retry = !retry;
      }
    } while (retry);
    fflush(_logFile);
  }
}
