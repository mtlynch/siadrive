#include <siaapi.h>
#include <regex>
#include <siadriveconfig.h>

using namespace Sia::Api;

#ifdef _WIN32
#include <tlhelp32.h>

static bool IsProcessRunning(const SString& processName) {
  bool exists = false;
  PROCESSENTRY32 entry;
  entry.dwSize = sizeof(PROCESSENTRY32);

  HANDLE snapshot = ::CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
  if (::Process32First(snapshot, &entry)) {
    while (!exists && ::Process32Next(snapshot, &entry)) {
      exists = SString(entry.szExeFile).EndsWith(processName);
    }
  }

  ::CloseHandle(snapshot);
  return exists;
}
#else

a
#endif

CSiaApi::CSiaApi(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  _siaDriveConfig(siaDriveConfig),
  _asyncServerVersion(""),
  _wallet(new CSiaWallet(siaDriveConfig)),
  _renter(new CSiaRenter(siaDriveConfig)),
  _consensus(new CSiaConsensus(siaDriveConfig)),
  _hostDb(new CSiaHostDb(siaDriveConfig)),
  _refreshThread(new CAutoThread(siaDriveConfig, [this](std::shared_ptr<CSiaDriveConfig> siaDriveConfig) {
                                   this->Refresh();
                                 })) {
}

CSiaApi::~CSiaApi() {
  _refreshThread->StopAutoThread();
  _asyncServerVersion = "";
}

void CSiaApi::Refresh() {
  _asyncServerVersion = GetServerVersion();
  this->_consensus->Refresh();
  this->_renter->Refresh();
  this->_hostDb->Refresh();
  // Initialize last - wallet contains connection status
  this->_wallet->Refresh();
}

SString CSiaApi::FormatToSiaPath(SString path) {
  if (path.Length()) {
    if (path.BeginsWith(".")) {
      path = path.SubString(1);
    }

    std::replace(path.begin(), path.end(), '\\', '/');
    regex r(SString::ActiveString("/+"));
    path = std::regex_replace(path.str(), r, SString::ActiveString("/"));

    while (path[0] == '/') {
      path = path.SubString(1);
    }
  }
  else {
    path = L"/";
  }

  return
    path;
}

void CSiaApi::StartBackgroundRefresh() {
  _refreshThread->StartAutoThread();
}

void CSiaApi::StopBackgroundRefresh() {
  _refreshThread->StopAutoThread();
}

SString CSiaApi::GetServerVersion() const {
  return CSiaCurl(_siaDriveConfig->GetHostConfig()).GetServerVersion();
}

CSiaWalletPtr CSiaApi::GetWallet() const {
  return _wallet;
}

CSiaRenterPtr CSiaApi::GetRenter() const {
  return _renter;
}

CSiaConsensusPtr CSiaApi::GetConsensus() const {
  return _consensus;
}

CSiaHostDbPtr CSiaApi::GetHostDb() const {
  return _hostDb;
}

bool CSiaApi::GetAllowLaunchBundledSiad() const {
#ifdef _WIN32
  return !IsProcessRunning("siad.exe") && _asyncServerVersion.IsNullOrEmpty();
#else
  return !IsProcessRunning("siad") && _asyncServerVersion.IsNullOrEmpty();
#endif
}
