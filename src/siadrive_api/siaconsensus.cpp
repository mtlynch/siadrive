#include <siaapi.h>
#include <siadriveconfig.h>

using namespace Sia::Api;

CSiaApi::_CSiaConsensus::_CSiaConsensus(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  CSiaBase(siaDriveConfig),
  _Height(0),
  _Synced(false),
  _CurrentBlock(L"") {
}

CSiaApi::_CSiaConsensus::~_CSiaConsensus() {
}

void CSiaApi::_CSiaConsensus::Refresh() {
  json result;
  if (ApiSuccess(CSiaCurl(GetSiaDriveConfig()->GetHostConfig()).Get(L"/consensus", result))) {
    SetHeight(result["height"].get<std::uint64_t>());
    SetSynced(result["synced"].get<bool>());
    SetCurrentBlock(result["currentblock"].get<std::string>());
  }
  else {
    SetHeight(0);
    SetSynced(false);
    SetCurrentBlock(L"");
  }
}
