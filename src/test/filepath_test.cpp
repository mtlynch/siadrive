#include <gtest/gtest.h>
#include <filepath.h>

using namespace Sia::Api;

TEST(FilePath, PlatformDirectorySepIsCorrect) {
#ifdef _WIN32
  ASSERT_EQ('\\', FilePath::DirSep[0]);
  ASSERT_EQ('/', FilePath::NDirSep[0]);
#else
  ASSERT_EQ('/', FilePath::DirSep[0]);
  ASSERT_EQ('\\', FilePath::NDirSep[0]);
#endif
}

TEST(FilePath, EmptyConstruction) {
  FilePath fp;
  EXPECT_STREQ(SString("").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, InitialPathSepIsRetained) {
  const SString test = FilePath::DirSep + "cow" + FilePath::DirSep + "moose";
  FilePath fp(test);
  EXPECT_STREQ(test.str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, TrailingPathSepIsRemoved) {
  const SString test = FilePath::DirSep + "cow" + FilePath::DirSep + "moose" + FilePath::DirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatIncorrectSep) {
  const SString test = FilePath::NDirSep + "cow" + FilePath::NDirSep + "moose" + FilePath::NDirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleInitialSep) {
  const SString test = FilePath::DirSep + FilePath::DirSep + FilePath::DirSep + FilePath::DirSep + "cow" + FilePath::DirSep + "moose" + FilePath::DirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleMiddleSep) {
  const SString test = FilePath::DirSep + "cow" + FilePath::DirSep + FilePath::DirSep + FilePath::DirSep + FilePath::DirSep + "moose" + FilePath::DirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleEndSep) {
  const SString test = FilePath::DirSep + "cow" + FilePath::DirSep + "moose" + FilePath::DirSep + FilePath::DirSep + FilePath::DirSep + FilePath::DirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleSepThroughout) {
  const SString test = FilePath::DirSep + FilePath::DirSep + FilePath::DirSep + "cow" + FilePath::DirSep + FilePath::DirSep + "moose" + FilePath::DirSep + FilePath::DirSep + FilePath::DirSep + FilePath::DirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleIncorrectInitialSep) {
  const SString test = FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep + "cow" + FilePath::NDirSep + "moose" + FilePath::NDirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleIncorrectMiddleSep) {
  const SString test = FilePath::NDirSep + "cow" + FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep + "moose" + FilePath::NDirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleIncorrectEndSep) {
  const SString test = FilePath::NDirSep + "cow" + FilePath::NDirSep + "moose" + FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatMultipleIncorrectSepThroughout) {
  const SString test = FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep + "cow" + FilePath::NDirSep + FilePath::NDirSep + "moose" + FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep + FilePath::NDirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, FormatCorrectAndIncorrectMixedSep) {
  const SString test = FilePath::DirSep + FilePath::NDirSep + FilePath::DirSep + "cow" + FilePath::NDirSep + FilePath::DirSep + "moose" + FilePath::NDirSep + FilePath::NDirSep + FilePath::DirSep + FilePath::NDirSep;
  FilePath fp(test);
  EXPECT_STREQ(SString(FilePath::DirSep + "cow" + FilePath::DirSep + "moose").str().c_str(), &fp.GetPath()[0]);
}

TEST(FilePath, RelativeToAbsolutePath)
{
  const SString test = ".." + FilePath::DirSep + "cow" + FilePath::DirSep + "moose";
  FilePath fp(test);
  EXPECT_EQ(&fp, &fp.MakeAbsolute());
  ASSERT_GT(fp.GetPath().Length(), 2);
  EXPECT_TRUE(fp[0] != '.');
  EXPECT_TRUE(fp[1] != '.');
}