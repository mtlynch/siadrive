# SiaDrive
SiaDrive is a user-mode filesystem/storage driver for Windows 7/8/8.1/10, a FUSE filesystem implementation for Linux/OSX, and a light-weight Sia client for managing your wallet. Mount Sia to any available drive letter on Windows or any directory location on Linux/OSX.

Windows and Linux will be supported in the first release. OSX will follow soon after.

![alt tag](http://i.imgur.com/2QgR2zY.png)

![alt tag](http://i.imgur.com/J45bfm6.png)

# Windows Configuration Details
- Cache location (%localappdata%\SiaDrive\cache)
- It is recommended to use the bundled Sia version. Siad data location (%localappdata%\SiaDrive\data)
- Configuration file (%localappdata%\SiaDrive\siadriveconfig.json)
- Log location (%localappdata%\SiaDrive\logs\siadrive.log)

# Important Notes
- The wallet needs to be unlocked and running at all times, especially near contrtact renewal period. It's best to run SiaDrive on a computer that is powered-on most of the day.
- You will need to have as much local storage space as you would like to store in Sia cloud. This is mandatory until Sia repairs no longer require files to be present locally.
- It's best to store larger files (> 40MB), but not a requirement. Files under 40MB will still consumer ~40MB.
- It's best to store files that do not change frequently. This is not a requirement but it can become a bit costly. Modified files result in a delete and re-upload to Sia. Future versions of Sia should make this less of an issue as deleted space will be reclaimed on hosts.

# Bugs and Feature Requests
- [Submit Here](https://bitbucket.org/siaextensions/siadrive/issues?status=new&status=open)
- [Email](mailto:sia.extensions@gmail.com)

# Credits
- [Chromium Embedded Framework](https://bitbucket.org/chromiumembedded/cef)
- [Dokany](https://github.com/dokan-dev/dokany)
- [JSON for Modern C++](https://github.com/nlohmann/json)
- [Sia](https://sia.tech/)
- [SQLiteCpp](https://github.com/SRombauts/SQLiteCpp)
- [ThreadPool](https://github.com/progschj/ThreadPool)
- [ttmath](http://www.ttmath.org/)

# Tips
- BTC: 18FhNwu4vgun3AZxUav9ncVCCyxfH3H3GN
- IOC: iWQzvncBLA5aK6q9RMxagR2u6LAfSi4ogt
- SC: 916ca57b05e7cf5f068c9d04e73c84788d4a9bc963a7209deaf0b69a120130241205d8fd3834